<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

//* UPLOADS  *//
// define( 'ALLOW_UNFILTERED_UPLOADS', true );
 
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'spanature');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'I5r8J)p>9w|^+MR[KQyI172%VPcYrq3.dI6TU0NyCS1-ghg%5z5)Mt/(,2iNS))u');
define('SECURE_AUTH_KEY',  '5B$MjFzh6+:=x+9KON,<QZu7wtZ^l?Oi4;]c@~#=Sx+<o7evPNdZ+fpBL&l+kH{9');
define('LOGGED_IN_KEY',    '7c@=AfQH2-SRJsp1Z+3KkfFf$-5Btif3c_20Y6!zM+`$mCP)7X~ W?6|u%%fVi[z');
define('NONCE_KEY',        'nUm]|Sj<+vZD6X2woF.T1(h+-j.aDjP@NlDV^{at+dLNA*f:UUmMrb<J.=-+`3/B');
define('AUTH_SALT',        'gNi*?cBK6udm<n|!ZZ-gQjw$9)u>:]-hO!B3. o<lmu+3w JOqOP8i 0qUB)D`[y');
define('SECURE_AUTH_SALT', 'Qxk*a~9v|_L*<}@H)S,}UkfG)s)q35[(z*yo0p{Mo)Rn]Srkvcq$h]hXQ}+91k}Q');
define('LOGGED_IN_SALT',   'FV?Z)A8~,|%Gew@M]*+dy|c@j~;-kH0)p^o>0$4L<Jr+,4T@}-1$S#h.W0-NJxCa');
define('NONCE_SALT',       ':a Vv<rJZ863C4qW5P917zWxXAlGEo#^lJxR|]+%#-1}pl|&J&#_$WAJ=sQ#]qJ+');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sn_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
